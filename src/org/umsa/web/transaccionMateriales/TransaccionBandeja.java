package org.umsa.web.transaccionMateriales;

import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.umsa.ConnectADQUI.AdquiWSServiceLocator;
import org.umsa.ConnectADQUI.AdquiWS_PortType;

import org.umsa.domain.*;
import org.umsa.domain.logic.MiFacade;

public class TransaccionBandeja implements Controller {
        
  private MiFacade adqui;
 
  public void setAdqui(MiFacade adqui) {
    this.adqui = adqui;
  } 

  public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
    
//    Recuperamos las variables

    int gestion=(Integer) request.getSession().getAttribute("__sess_gestion");
    Clientes cliente = (Clientes) request.getSession().getAttribute("__sess_cliente");
    Roles rol = (Roles) request.getSession().getAttribute("__sess_rol");

    String cod_tramite =request.getParameter("cod_tramite");
    String cod_transaccion=request.getParameter("cod_transaccion");
    String cod_w =request.getParameter("cod_w");
    String seleccion=request.getParameter("metodo");
    String dias1=request.getParameter("dias1");
    String dias2=request.getParameter("dias2");
    String nombre=request.getParameter("nom");
    String cargo=request.getParameter("cargo");
    String correo=request.getParameter("correo");
    String Inspeccion=request.getParameter("id1");
    String Consulta=request.getParameter("id2");
    String Reunion=request.getParameter("id3");
    //System.out.println("esto: "+cod_transaccion+"a: "+seleccion+" b: "+nombre+" c: "+cargo+" d: "+correo+" e: "+Inspeccion+" f: "+Consulta+" g: "+Reunion);
    System.out.println("------------- :D cod_tramite: "+cod_tramite+" cod_w: "+cod_w);
    
    Transaccion extra=new Transaccion();
    //extra.setMetodo(seleccion);
    //extra.setResponsable(nombre);
    //extra.setCargo(cargo);
    //extra.setCorreo(correo);
    //int cod_transaccion=0;
    if( cod_transaccion == null)
    {
        //extra.setCod_transaccion(Integer.parseInt(cod_transaccion));
    }
    else
    {
        //extra.setCod_transaccion(Integer.parseInt(cod_transaccion));
        if(Inspeccion.equals("true"))
        {
            Inspeccion="checked";
            //extra.setInspeccion("checked");
        }
        else
        {
            Inspeccion="";
        }
        if(Consulta.equals("true"))
        {
            Consulta="checked";
            //extra.setConsultas("checked");
        }
        else
        {
            Consulta="";
        }
        if(Reunion.equals("true"))
        {
            Reunion="checked";
            //extra.setReunion("checked");
        }
        else
        {
            Reunion="";
        }
        try{
            AdquiWSServiceLocator servicio = new AdquiWSServiceLocator();
            AdquiWS_PortType puerto = servicio.getAdquiWS();
            puerto.updateTransaccion(seleccion,nombre,cargo,correo,Inspeccion,Consulta,Reunion,Integer.parseInt(cod_transaccion),dias1,dias2);
        }catch(Exception e)
        {
            
        }
        System.out.println("esto: "+extra.getCod_transaccion()+" a: "+seleccion+" b: "+nombre+" c: "+cargo+" d: "+correo+" e: "+Inspeccion+" f: "+Consulta+" g: "+Reunion);
        //cod_transaccion = this.adqui.setTransaccion(extra);
    }
    
    
    
    Transaccion trans= new Transaccion();
    System.out.println("Es --> "+cliente.getId_usuario());
    trans.setCod_usuario(cliente.getId_usuario());
    trans.setCod_tramite(Integer.parseInt(cod_tramite)); 
    trans.setGestion(gestion);
    trans.setCod_w(Integer.parseInt(cod_w));

    Items tt = new Items();
    tt.setCod_tramite(Integer.parseInt(cod_tramite));
    tt.setCod_almacen(cliente.getCod_almacen());
            
    String tipo_tramite=this.adqui.getTipoTramite(tt);

    //PagedListHolder listaBandeja = new PagedListHolder(this.adqui.getTransaccionMateriales(trans));
    PagedListHolder listaBandeja = new PagedListHolder(this.adqui.getTransaccionSolicitudes(trans));
    listaBandeja.setPageSize(listaBandeja.getNrOfElements());    
    
    
    PagedListHolder listaEnviados = new PagedListHolder(this.adqui.getEnviados2(trans));
    listaEnviados.setPageSize(listaEnviados.getNrOfElements());
    System.out.println("OOOOOOOOOO nro elementos: "+listaEnviados.getNrOfElements());
    
    
    
    Map modelo = new HashMap();

    modelo.put("ListBandeja",listaBandeja);
    modelo.put("listaEnviados",listaEnviados);
    modelo.put("tipo_tramite",tipo_tramite);
    modelo.put("cod_tramite",cod_tramite);
    modelo.put("cod_w",cod_w);
    Operaciones o = new Operaciones();
    o.setCod_transaccion(String.valueOf(10)); 
    PagedListHolder listaAdjuntos = new PagedListHolder(this.adqui.ListaAdjuntos(o));
    listaAdjuntos.setPageSize(listaAdjuntos.getNrOfElements());
    modelo.put("listaAdjuntos", listaAdjuntos);
    return new ModelAndView("transaccionMateriales/TransaccionBandeja", modelo);

  }
}