/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.umsa.web.transaccionMateriales;

import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.umsa.ConnectADQUI.AdquiWSServiceLocator;
import org.umsa.ConnectADQUI.AdquiWS_PortType;
import org.umsa.domain.Clientes;
import org.umsa.domain.Operaciones;
import org.umsa.domain.Transaccion;
import org.umsa.domain.logic.MiFacade;

/**
 *
 * @author alex
 */
public class ArchivoNuevo implements Controller{

    private MiFacade adqui;

    public void setAdqui(MiFacade adqui) {
      this.adqui = adqui;
    }
    
    @Override
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse hsr1) throws Exception {
        Map modelo = new HashMap();
        String descripcion=request.getParameter("txtdesc_");
        String fecha=request.getParameter("txtfecha_");
        String tipo=request.getParameter("tipo_doc");
        System.out.println("desc "+descripcion+" fec "+fecha+" tipo "+tipo);
        
        /*PagedListHolder listaTipoAdj = new PagedListHolder(this.adqui.GetTiposADJ());
        listaTipoAdj.setPageSize(listaTipoAdj.getNrOfElements());
        System.out.println("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa "+listaTipoAdj.getNrOfElements());
        modelo.put("listaTipoAdj",listaTipoAdj);*/
        
        
        /*****************realizando el modelo***********//////
        String tipo_transaccion=request.getParameter("tipo_transaccion");
        
        String tipo_tramite = request.getParameter("tipo_tramite");
        String cod_tramite = request.getParameter("cod_tramite");
        String cod_w =request.getParameter("cod_w");
        String cod_transaccion =request.getParameter("cod_transaccion");
        
        String descripcion_doc=request.getParameter("descripcion_doc");
        //String descripcion=request.getParameter("descripcion_");
        System.out.println("wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww"+descripcion);
        
        String tipo_doc = request.getParameter("tipo_doc");
        
        
        Operaciones o = new Operaciones();
        o.setGestion("2016");
        o.setCod_transaccion(cod_transaccion);
        
        
        System.out.println("Datos --> "+tipo_tramite);
        System.out.println("Datos --> "+cod_tramite);
        System.out.println("Datos --> "+cod_w);
        System.out.println("Datos --> "+cod_transaccion);
        System.out.println("Datos --> "+descripcion_doc);
        System.out.println("Tipo Doc  --> "+tipo_doc);
        
        Clientes cliente=new Clientes();
        cliente=(Clientes) request.getSession().getAttribute("__sess_cliente");

        Transaccion trans = new Transaccion();        
        trans.setCod_transaccion(Integer.parseInt(cod_transaccion));
        trans.setCod_tramite(Integer.parseInt(cod_tramite));
        trans.setCod_almacen(cliente.getCod_almacen());        
        Transaccion items = this.adqui.getTransaccionMaterial(trans);  
      

            modelo.put("tipo_tramite",tipo_tramite );           
            modelo.put("cod_transaccion",cod_transaccion);
            modelo.put("cod_w",cod_w);
            modelo.put("cod_tramite",cod_tramite);

            modelo.put("usuario_sol",items.getUsuario_sol() );
            modelo.put("detalle",items.getDetalle());
            modelo.put("ue_solicitante",items.getUnidad_sol() );
            modelo.put("ue_destino",items.getUnidad_des() );
            modelo.put("cod_transaccion",items.getCod_transaccion());
            modelo.put("nro_gestion",items.getNro_gestion());
            
            modelo.put("descripcion_deb",descripcion);
            modelo.put("fecha_deb",fecha);
            modelo.put("tipo_deb",tipo);
            
            
            
            PagedListHolder listaTipoAdj = new PagedListHolder(this.adqui.GetTiposADJ());
            listaTipoAdj.setPageSize(listaTipoAdj.getNrOfElements());
            System.out.println("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa "+listaTipoAdj.getNrOfElements());
            modelo.put("listaTipoAdj",listaTipoAdj);
            
            /*******segunda parte********/
            PagedListHolder listaAdjuntos = new PagedListHolder(this.adqui.ListaAdjuntos(o));
                listaAdjuntos.setPageSize(listaAdjuntos.getNrOfElements());            
                modelo.put("listaAdjuntos",listaAdjuntos);

                PagedListHolder lista_partidas = new PagedListHolder(this.adqui.GetPartidas(o));
                lista_partidas.setPageSize(lista_partidas.getNrOfElements());            
                modelo.put("lista_partidas",lista_partidas);

                PagedListHolder listaTransaccionArticulos = new PagedListHolder(this.adqui.getTransaccionArticulos2(Integer.parseInt(cod_transaccion)));
                listaTransaccionArticulos.setPageSize(listaTransaccionArticulos.getNrOfElements());
//               listaTransaccionArticulos.getNrOfElements()
                modelo.put("listaTransaccionArticulos",listaTransaccionArticulos);

                PagedListHolder ListaUM = new PagedListHolder(this.adqui.getUnidadesMedida());
                ListaUM.setPageSize(ListaUM.getNrOfElements());
                modelo.put("ListaUM",ListaUM);

                PagedListHolder listaDocs = new PagedListHolder(this.adqui.getAdjuntos(Integer.parseInt(cod_transaccion)));
                listaDocs.setPageSize(listaDocs.getNrOfElements());
                modelo.put("listaDocs",listaDocs);
                modelo.put("listaTransaccionArticulos",listaTransaccionArticulos);
                modelo.put("cuantia",items.getCuantia());
                try{
                    AdquiWSServiceLocator servicio = new AdquiWSServiceLocator();
                    AdquiWS_PortType puerto = servicio.getAdquiWS();
                    Map[] datos= puerto.getTransaccion2(Integer.parseInt(cod_transaccion));
                    modelo.put("metodo",datos[0].get("METODO"));
                    modelo.put("nom",datos[0].get("RESPONSABLE"));
                    modelo.put("cargo",datos[0].get("CARGO"));
                    modelo.put("correo",datos[0].get("CORREO"));
                    modelo.put("uno",datos[0].get("INSPECCION"));
                    modelo.put("dos",datos[0].get("CONSULTA"));
                    modelo.put("tres",datos[0].get("REUNION"));
                    modelo.put("dias1", datos[0].get("DIAS1"));
                    modelo.put("dias2", datos[0].get("DIAS2"));
                }catch(Exception e)
                {

                }
          
                PagedListHolder listaUE = new PagedListHolder(this.adqui.getUnidadEjecutora(cliente));
            listaUE.setPageSize(listaUE.getNrOfElements());
            modelo.put("listaUE", listaUE);
        return new ModelAndView("transaccionMateriales/Transaccion2", modelo);
        //return null;
        //throw new UnsupportedOperationException("Not supported yet.");
    }
    
}
