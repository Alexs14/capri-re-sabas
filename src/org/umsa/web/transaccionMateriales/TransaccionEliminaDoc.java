/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.umsa.web.transaccionMateriales;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.umsa.ConnectADQUI.AdquiWSServiceLocator;
import org.umsa.ConnectADQUI.AdquiWS_PortType;
import org.umsa.domain.Clientes;
import org.umsa.domain.Operaciones;
import org.umsa.domain.logic.MiFacade;

/**
 *
 * @author julian
 */
public class TransaccionEliminaDoc implements Controller {

    private MiFacade adqui;

    public void setAdqui(MiFacade adqui) {
        this.adqui = adqui;
    }

    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String tipo_tramite =request.getParameter("tipo_tramite");
        String usuario_sol =request.getParameter("usuario_sol");
        String detalle =request.getParameter("detalle");
        String ue_solicitante =request.getParameter("ue_solicitante");
        String ue_destino =request.getParameter("ue_destino");        
        String cod_transaccion =request.getParameter("cod_transaccion");        
        String cod_tramite =request.getParameter("cod_tramite");
        String nro_gestion =request.getParameter("nro_gestion");        
        String cod_w =request.getParameter("cod_w");        
        String cod_docs =request.getParameter("cod_docs");
        String cuantia=request.getParameter("cuantia");
        
        
        String tipo_transaccion=request.getParameter("tipo_transaccion");
        
        String descripcion_doc=request.getParameter("descripcion_doc");
        
        
        int gestion=(Integer) request.getSession().getAttribute("__sess_gestion");
        Clientes cliente=new Clientes();
        cliente=(Clientes) request.getSession().getAttribute("__sess_cliente");        
        cliente.setGestion(gestion);
        int nro_trans = this.adqui.getNroTransaccion(cliente);
        Map modelo = new HashMap();

        this.adqui.delTransaccionTerminos(Integer.parseInt(cod_docs));
        
        PagedListHolder listaTransaccionArticulos = new PagedListHolder(this.adqui.getTransaccionArticulos2(Integer.parseInt(cod_transaccion)));
        listaTransaccionArticulos.setPageSize(listaTransaccionArticulos.getNrOfElements());
        modelo.put("listaTransaccionArticulos",listaTransaccionArticulos);
        
        PagedListHolder listaDocs = new PagedListHolder(this.adqui.getAdjuntos(Integer.parseInt(cod_transaccion)));
        listaDocs.setPageSize(listaDocs.getNrOfElements());
        modelo.put("listaDocs",listaDocs);
        
        Operaciones o = new Operaciones();
        o.setGestion("2014");
        o.setCod_transaccion(cod_transaccion);
        
        PagedListHolder lista_partidas = new PagedListHolder(this.adqui.GetPartidas(o));
        lista_partidas.setPageSize(lista_partidas.getNrOfElements());            
        modelo.put("lista_partidas",lista_partidas);
        
        PagedListHolder ListaUM = new PagedListHolder(this.adqui.getUnidadesMedida());
        ListaUM.setPageSize(ListaUM.getNrOfElements());
        modelo.put("ListaUM",ListaUM);
        
        PagedListHolder listaAdjuntos = new PagedListHolder(this.adqui.ListaAdjuntos(o));
        listaAdjuntos.setPageSize(listaAdjuntos.getNrOfElements());            
        modelo.put("listaAdjuntos",listaAdjuntos);
        
        PagedListHolder listaTipoAdj = new PagedListHolder(this.adqui.GetTiposADJ());
        listaTipoAdj.setPageSize(listaTipoAdj.getNrOfElements());
        System.out.println("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa "+listaTipoAdj.getNrOfElements());
        modelo.put("listaTipoAdj",listaTipoAdj);
            
        String ruta ="";
        if (Integer.parseInt(cod_w)==3){            
            List item = null;
            //if (listaTransaccionArticulos.getNrOfElements()==0 )
                item = this.adqui.getBuscaConsultoresObras(4);
            modelo.put("items",item);
            System.out.println("Sale Consultor --> "+cod_w);
            //ruta="transaccionMateriales/TransaccionConsultoresDetalle";
            ruta="transaccionMateriales/Transaccion2";
        }
        else if (Integer.parseInt(cod_w)==4){               
            List item = null;
            //if (listaTransaccionArticulos.getNrOfElements()==0 )
                item = this.adqui.getBuscaConsultoresObras(5);
            modelo.put("items",item);
            
            System.out.println("Sale Obras --> "+cod_w);
            //ruta="transaccionMateriales/TransaccionObrasDetalle";
            ruta="transaccionMateriales/Transaccion2";
        }
        else {
            System.out.println("Sale Items --> "+cod_w);
            //ruta="transaccionMateriales/TransaccionItems";
            ruta="transaccionMateriales/Transaccion2";
        }                          
        
        modelo.put("tipo_tramite",tipo_tramite );
        modelo.put("usuario_sol",usuario_sol );
        modelo.put("detalle",detalle );
        modelo.put("ue_solicitante",ue_solicitante );
        modelo.put("ue_destino",ue_destino );
        modelo.put("cod_transaccion",cod_transaccion);        
        modelo.put("cod_tramite",cod_tramite);
        modelo.put("cod_w",cod_w);
        modelo.put("nro_gestion",nro_gestion);
        modelo.put("cuantia",cuantia);
                modelo.put("descripcion_deb","");
                modelo.put("fecha_deb","");
                modelo.put("tipo_deb",1);
                try{
                    AdquiWSServiceLocator servicio = new AdquiWSServiceLocator();
                    AdquiWS_PortType puerto = servicio.getAdquiWS();
                    Map[] datos= puerto.getTransaccion2(Integer.parseInt(cod_transaccion));
                    modelo.put("metodo",datos[0].get("METODO"));
                    modelo.put("nom",datos[0].get("RESPONSABLE"));
                    modelo.put("cargo",datos[0].get("CARGO"));
                    modelo.put("correo",datos[0].get("CORREO"));
                    modelo.put("uno",datos[0].get("INSPECCION"));
                    modelo.put("dos",datos[0].get("CONSULTA"));
                    modelo.put("tres",datos[0].get("REUNION"));
                    modelo.put("dias1", datos[0].get("DIAS1"));
                    modelo.put("dias2", datos[0].get("DIAS2"));
                }catch(Exception e)
                {

                }          
        
        PagedListHolder listaUE = new PagedListHolder(this.adqui.getUnidadEjecutora(cliente));
            listaUE.setPageSize(listaUE.getNrOfElements());
            modelo.put("listaUE", listaUE);
        return new ModelAndView(ruta, modelo);
    }
}
