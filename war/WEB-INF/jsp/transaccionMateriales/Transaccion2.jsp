<%-- 
    Document   : Transaccion2
    Created on : 19-08-2014, 09:14:24 AM
    Author     : UMSA-JES
--%>
<%@ include file="../Superior.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <script>
           $(document).on("ready",inicia);
           function inicia(){
              archivos_adjuntos();
              iniciaTabla();
           }
           function archivos_adjuntos(){
              
                $("#btn_archivo_adjunto").on("click",function(){
                   //alert("Hola :P");
                   if( ($("#archivo_adjunto").val()!=='') && ($("#descripcion_doc").val()!=='') && ($("#tipo_doc").val()!=='')){
                       //alert("Hola :P 2");
                       var x = $("#descripcion_doc").val();
                       var y = $("#tipo_doc").val();
                       $("#formu_prob").attr("action",'<c:url value="/TransaccionConsultorAdjTerminos.umsa">
                                <c:param name="cod_tramite" value="${cod_tramite}"/>                            
                                <c:param name="cod_w" value="${cod_w}"/> 	      
                                <c:param name="cod_transaccion" value="${cod_transaccion}"/> 
                                <c:param name="tipo_tramite" value="${tipo_tramite}"/>
                                <c:param name="tipo_transaccion" value="solicitud_compras"/> 
                        </c:url>'+"&descripcion_doc="+x+"&tipo_doc="+y);
                       $("#formu_prob").submit();
                            
                   }
                   else
                   {
                       alert("Debe introducir información completa, concerniente al archivo adjunto");
                   }
                      
                });
           }
           function setBuscar(){
                var texto = document.getElementById('search');
                if (texto.value.length <2){
                    alert('Debes ingresar algun dato para buscar.');
                    return false;
                }
                else { 
                    document.getElementById('Buscar').value = 'Buscando...';
                    return true;

                }
            }

            function setRegistrar(f) {
                if(isNaN(parseInt(f.cantidad.value))|| parseInt(f.cantidad.value)==0)  {
                    alert('La cantidad debe ser un numero mayor a cero'); return false;
                }
                
                f.guardar.disabled = true;
                f.guardar.value = 'Enviando los datos...';
                /*si los datos no son correctos */
                return true;
            }
            /*=====================================================
             * ABRE UN POPUP INTERNO EN EL FORMULARIO PRINCIPAL
             =======================================================*/
            function openAddForm(form,title,w,h){
                var addFormwin=dhtmlmodal.open('win'+form, 'div', form, title, 'width='+w+'px,height='+h+'px,left=100px,top=100px,resize=1,scrolling=1');
                addFormwin.moveTo('middle', 'middle');
                return addFormwin;
            }
            
            var popup = null;
            var id_ia=null;
            function CantidadItem(id,unidad) {
                //alert(id+" "+unidad);
                id_ia=id;
                popup = openAddForm('popup','Colocar Cantidad',250, 30);
                document.getElementById('cod_item').value = id_ia;
                document.getElementById('unidad_medida').value = unidad;                
                return true;
            }
            /*====================================================================
             * COLOCA COLOR A LA FILA QUE SE ELIJE EN UNA TABLA
             ===================================================================*/
            function ini() {
              tab=document.getElementById('tabla');
              for (i=0; ele=tab.getElementsByTagName('td')[i]; i++) {	
                  if (i>2) {
                    ele.onmouseover = function() {iluminar(this,true)}
                    ele.onmouseout = function() {iluminar(this,false)} 
                  }
              }
            }

            function iluminar(obj,valor) {
              fila = obj.parentNode;
                for (i=0; ele = fila.getElementsByTagName('td')[i]; i++)                   
                  ele.style.background = (valor) ? '#c5d6e7' : 'white';              
            }
            function habilita()
            {
                var a=document.getElementById('txtdesc_').value;
                var b=document.getElementById('nc_fecha').value;
                    if(a!="" && b!="")
                    {
                        document.getElementById('tipo_doc').disabled=false;
                    }
                    else
                    {
                        document.getElementById('tipo_doc').disabled=true;
                    }
                    //alert("si entra aqui");
            }
            function envia_datos()
            {
                var a=document.getElementById('txtdesc_').value;
                var b=document.getElementById('nc_fecha').value;
                var cc=document.getElementById('tipo_doc').value;
                //var c=cc.option[cc.selectedIndex].value;
                var dir = "/capri-web/TransaccionArchivo.umsa?txtdesc_="+a+"&txtfecha_="+b+"&tipo_doc="+cc+"&cod_tramite=${cod_tramite}&cod_w=${cod_w}&cod_transaccion=${cod_transaccion}&tipo_tramite=${tipo_tramite}&tipo_doc=${listaAdjunto.cod_adjunto}&tipo_transaccion=solicitud_compras";
                //dir += document.getElementById('cmbCursos').value;*/
                //alert("si entra aqui "+ dir );
                window.location.href=dir;
            }
            function datosAnpe(cuantia)
            {
                var xx=cuantia;
                //alert("asdasdasd12222222"+xx);
                if(xx=="COMPRA MENOR")
                {
                    var dir="/capri-web/TransaccionMateriales.umsa?cod_tramite=1&cod_w=${cod_w}";
                }
                else
                {
                    var a=document.getElementById('metodo').value;
                    var a1=document.getElementById('dias1').value;
                    //var a2=document.getElementById('dias2').value;
                    var b=document.getElementById('nom').value;
                    var c=document.getElementById('cargo').value;
                    var d=document.getElementById('correo').value;
                    if(xx=="ANPE")
                    {
                        var e="";
                        var f="";
                        var gg="";
                    }
                    else
                    {
                        var e=document.getElementById('id1').checked;
                        var f=document.getElementById('id2').checked;
                        var gg=document.getElementById('id3').checked;
                    }
                    var dir="/capri-web/TransaccionMateriales.umsa?cod_tramite=1&metodo="+a+"&nom="+b+"&cargo="+c+"&correo="+d+"&id1="+e+"&id2="+f+"&id3="+gg+"&dias1="+a1+"&dias2=&cod_w=${cod_w}&cod_transaccion=${cod_transaccion}";
                }
                
                //var dir="/capri-web/TransaccionMateriales.umsa?cod_tramite=1&cod_w=${cod_w}";
                window.location.href=dir;
            }
        </script>
    </head>
    <body>
        <header>
            
            <div class="box">
                <h2><a href="#" id="toggle-forms">Gestion: ${nro_gestion} -- Tipo Cuantia: ${cuantia}</a><!--button id="btn-poa" data-nro_gestion="${nro_gestion}" data-ue_solicitante="${ue_solicitante}">POA - Unidad Solicitante</button--><button id="btn-poa_padre" data-nro_gestion="${nro_gestion}" data-ue_solicitante="${ue_padre}">POA - UNIDAD</button></h2>
            </div>
            <div class="box" id="detalle_header">
                <form action="/capri-web/ModifDatosIni.umsa" method="POST">
                <div>
                    <p><span class="verde">Usuario Solicitante:</span> <span class="celeste"><input type="text" name="usuario_sol" value="${usuario_sol}"/></span></p>
                    <p><span class="verde">Detalle Solicitud:</span> <span class="celeste"><input type="text" name="detalle" value="${detalle}"/></span></p>
                </div>
                <div>
                    <p><span class="verde">Unidad Solicitante:</span> <span class="celeste">
                            <select name="ue_solicitante2" style="width: 300px" required>
                                    
                                  <c:forEach var="lista" items="${listaUE.pageList}">
                                    <c:if test="${lista.apert_prog==ue_solicitante}">
                                    <option selected value="<c:out value="${lista.apert_prog}"/>">
                                        <c:out value="${lista.apert_prog}"/>
                                    </option>
                                    </c:if>
                                    <option value="<c:out value="${lista.apert_prog}"/>">
                                        <c:out value="${lista.apert_prog}"/>
                                    </option>
                                  </c:forEach>
                        </select>
                        </span></p>
                        <p><span class="verde">Unidad Destino:</span> <span class="celeste"><input type="text" name="unidad_des" value="${ue_destino}"/></span></p>
                        <input type="hidden" name="cod_transaccion" value="<c:out value="${cod_transaccion}"/>"/>
                        <input type="hidden" name="tipo_tramite" value="<c:out value="${tipo_tramite}"/>"/>
                        <input type="hidden" name="cod_tramite" value="<c:out value="${cod_tramite}"/>"/>
                        <input type="hidden" name="cod_w" value="<c:out value="${cod_w}"/>"/>
                        <input type="hidden" name="nro" value="<c:out value="${nro}"/>"/>
                </div>
                <div>
                    <input type="submit" value="MODIFICAR CABEZERA">
                </div>
                </form>
            </div>
        </header>
        
            <%--c:out value="${cod_w}"/--%>    
            <%--c:if test="${cod_w != 3 and cod_w != 4}"--%>
                <div id="cuerpo_detalle_solicitud" class="div_tabla_items">
                <table class="table table-striped" id="mainTable">
                    <thead>
                        <tr>
                            <th>N°</th>
                            <th>Cantidad</th>
                            <th>Unidad Medida</th>
                            <th>Articulo</th>
                            <th>Partida</th>
                            <th>Precio U.</th>
                            <th>Subtotal</th>
                            <th>Opciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach var="lista" items="${listaTransaccionArticulos.pageList}" varStatus="contador">
                            <tr data-cod_trans_detalle="<c:out value="${lista.cod_trans_detalle}"/>">
                                <td>${ listaTransaccionArticulos.getNrOfElements()-(contador.index)} </td>
                                <td class="cantidad"><input required type="number" min="1" max="9999999" size="5" value="<c:out value="${lista.cantidad}"/>"/></td>
                                <td class="unidad_medida">
                                    <select name="unidad_medida">
                                        
                                        <c:forEach var="lista2" items="${ListaUM.pageList}">
                                            <c:if test="${lista.cod_unidad_medida==lista2.cod_unidad_medida}">
                                                <option selected value="<c:out value="${lista2.cod_unidad_medida}"/>">
                                                    <c:out value="${lista2.detalle}"/>
                                                </option>
                                            </c:if>
                                            <c:if test="${lista.cod_unidad_medida!=lista2.cod_unidad_medida}">
                                                <option value="<c:out value="${lista2.cod_unidad_medida}"/>">
                                                    <c:out value="${lista2.detalle}"/>
                                                </option>
                                            </c:if>
                                        </c:forEach>
                                    </select>
                                </td>
                                <td class="item">
                                    <input id="Input<c:out value="${lista.cod_trans_detalle}"/>" list="List<c:out value="${lista.cod_trans_detalle}"/>" required type="text" maxlength="55" size="65" value="<c:out value="${lista.detalle}"/>"/>
                                    <datalist class="dataList" id="List<c:out value="${lista.cod_trans_detalle}"/>">
                                        <!--option label="changos" value="changos"/-->
                                    </datalist>
                                </td>
                                <td id="Part<c:out value="${lista.cod_trans_detalle}"/>"><c:out value="${lista.partida}"/></td>
                                <td class="precio"><input size="10" type="text" placeholder="Ingrese precio" value="<c:out value="${lista.precio_unit}"/>"/></td>
                                <td class="subtotal"> -- </td>
                                <td><a class="delArticulo" href="#"><img class="img_peque" src="imagenes/minus.png"/></a><a class="detailItem" href="#"><img class="img_peque" src="imagenes/detail.png"/></a></td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
                </div>
                <div class="opciones_genera">
                    <c:if test="${cod_w != 3 and cod_w != 4}">
                        <a id="addArticulo" href="#" data-cod_transaccion="${cod_transaccion}" data-gestion="2016" ><img class="img_mediana" src="imagenes/plus.png" alt="add"/></a>
                        <a id="vistaPrevia" href="<c:url value="/reporteSolicitudBorrador"/>?&tipo_tramite=${1}&cod_transaccion=${cod_transaccion}&cod_estado=PPTO " target="_blank"><img class="img_mediana" src="imagenes/vista_previa.png" alt="Vista Previa"/></a>
                    </c:if>
                    <label class="label_total">TOTAL: <span id="total">0</span> Bs</label> 
                </div>
            <%--/c:if--%>
            <%--c:if test="${cod_w == 3 or cod_w == 4}">
                hola
            </c:if--%>
                
                
                <!--label>Superhéroe favorito</label>
                <input list="superheroes" name="list" />
                <datalist id="superheroes">
                    <option label="Iron Man" value="Iron Man">
                    <option label="Iron Girl" value="Iron Girl">
                    <option label="The Hulk" value="The Hulk">
                </datalist-->
                
                <!--input list="items" id="item">

                <datalist id="items">
                  <option value="A item"  data-xyz = "1" >
                  <option value="aa item" data-xyz = "2" >
                  <option value="C item"  data-xyz = "3" >
                  <option value="D item"  data-xyz = "4" >
                  <option value="E item"  data-xyz = "5" >
                </datalist--> 

                <!--input type="button" id="button" value="Get xyz" /-->

            
        
        <div id="cuerpo_archivos_adjuntos" class="div_tabla_adjuntos">
            <table>
                <thead>
                    <th>Archivos Adjuntos.</th>
                    <th>Descripción</th>
                    <th>Tipo</th>
                    <th>Opciones</th>
                </thead>
                <tbody>
                    <c:forEach var="lista" items="${listaDocs.pageList}" varStatus="contador">
                        <c:if test="${lista.tipo_item == null }">
                            <tr class="odd">
                                <td>
                                    <a target="_blank" href="<c:url value="../prueba/${lista.terminos_ref}" ></c:url>" style="color:green">${lista.terminos_ref}</a> 
                                </td>
                                <td>
                                    <p>${lista.descripcion}</p>
                                </td>
                                <td>
                                    <p>${lista.tipo_doc}</p>
                                </td>
                                <c:if test="${lista.cod_docs != null }">
                                    <td>
                                        <a href="<c:url value="/TransaccionEliminaDoc.umsa">
                                           <c:param name="cod_tramite" value="${cod_tramite}"/>
                                           <c:param name="tipo_tramite" value="${tipo_tramite}"/>
                                           <c:param name="usuario_sol" value="${usuario_sol}"/>
                                           <c:param name="detalle" value="${detalle}"/>
                                           <c:param name="ue_solicitante" value="${ue_solicitante}"/>
                                           <c:param name="ue_destino" value="${ue_destino}"/>  
                                           <c:param name="nro_gestion" value="${nro_gestion}"/>
                                           <c:param name="cod_w" value="${cod_w}"/>
                                           <c:param name="cod_docs" value="${lista.cod_docs}"/>
                                           <c:param name="cod_transaccion" value="${cod_transaccion}"/>
                                           <c:param name="cuantia" value="${cuantia}"/>
                                        </c:url>" onclick="javascript:return confirm('¿Esta seguro(a) que desea eliminar el registro?')" style="color:green">Eliminar</a>  
                                    </td>
                                </c:if>
                            </tr>
                        </c:if>
                    </c:forEach>
                    <c:if test="${!empty listaTransaccionArticulos.pageList}">
                            <tr>
                                <td colspan="6">
                                   
                                </td>
                            </tr>
                        </c:if>
                    </tbody>
                </table>
            </div>
           
                
           <div>
               
                <p><span style="color:red"><c:out value="${error}"/></span></p>
                
                
                <table>
                    <thead>
                        <tr>
                            <th>Fecha</th>
                            <th>Descripcion</th>
                            <th>Tipo</th>
                            <th>Detalle</th>
                            <th>Opciones</th>
                            
                        </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td><input type="text" name="nc_fecha" id="nc_fecha" onkeyup=habilita() value="${fecha_deb}"/></td>
                        <td><input type="text" name="txtdesc_" id="txtdesc_" onkeyup=habilita() value="${descripcion_deb}"/></td>
                            <td>
                                <select id="tipo_doc" name="tipo_doc" disabled="true" onchange=envia_datos()>
                                    <option>Seleccione un tipo de Documento</option>
                                    <c:forEach var="listaTipoAdj" items="${listaTipoAdj.pageList}" varStatus="contador">
                                            <c:if test="${listaTipoAdj.cod_adjunto==tipo_deb}">
                                                <option selected value="<c:out value="${listaTipoAdj.cod_adjunto}"/>">
                                                    <c:out value="${listaTipoAdj.descripcion}" />
                                                </option>
                                            </c:if>
                                            <c:if test="${listaTipoAdj.cod_adjunto!=tipo_deb}">
                                                <option value="<c:out value="${listaTipoAdj.cod_adjunto}" />"><c:out value="${listaTipoAdj.descripcion}" /></option>
                                            </c:if>
                                    </c:forEach>
                                </select>

                            </td>
                            <c:forEach var="listaAdjuntos" items="${listaAdjuntos.pageList}" varStatus="contador">
                                <form action='/capri-web/TransaccionConsultorAdjTerminos.umsa?cod_tramite=${cod_tramite}&cod_w=${cod_w}&cod_transaccion=${cod_transaccion}&tipo_tramite=${tipo_tramite}&tipo_doc=${tipo_deb}&desc_=${descripcion_deb}&fecha_=${fecha_deb}&tipo_transaccion="solicitud_compras"' method="POST" enctype="multipart/form-data">
                                        <td><input type="file" name="archivo" size="60" id="archivo_adjunto"/></td>
                                        <td><input type="submit" value="ADD"/></td>

                                </form>

                            </c:forEach>
                    </tr>
                    </tbody>
                
                    
                </table>
            </div>
            <c:if test="${cuantia == 'ANPE' || cuantia == 'ANPE > 200000'}">        
                <div>
                    <table>
                        <thead>
                            <tr>
                                <th></th>
                                <th>DEBE LLENAR ESTOS DATOS SI ES UN TRAMITE ANPE</th>
                                <c:if test="${cuantia=='ANPE > 200000'}">
                                <th></th>
                                </c:if>
                            </tr>
                            <tr>
                                <th>metodo de seleccion y adjudicacion</th>
                                <th>Funcionario a cargo de atender consultas y solicitudes de aclaracion</th>
                                <c:if test="${cuantia=='ANPE > 200000'}">
                                <th>Actividades previas</th>
                                </c:if>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <%--td><input type="text" id="metodo" name="metodo" placeholder="METODO" value="${metodo}"--%>
                                <td><select name="metodo" id="metodo">
                                        <c:if test="${cod_w==1}">
                                                <option value="Calidad Propuesta Tecnica y Costo">Calidad Propuesta Tecnica y Costo</option>
                                                <option value="Calidad">Calidad</option>
                                                <option value="Precio Evaluado mas Bajo">Precio Evaluado mas Bajo</option>
                                        </c:if>
                                        <c:if test="${cod_w==6}">
                                                <option value="Calidad Propuesta Tecnica y Costo">Calidad Propuesta Tecnica y Costo</option>
                                                <option value="Precio Evaluado mas Bajo">Precio Evaluado mas Bajo</option>
                                        </c:if>
                                        <c:if test="${cod_w==3}">
                                                <option value="Calidad Propuesta Tecnica y Costo">Calidad Propuesta Tecnica y Costo</option>
                                                <option value="Calidad">Calidad</option>
                                                <option value="Presupuesto Fijo">Presupuesto Fijo</option>
                                        </c:if>
                                        <c:if test="${cod_w==4}">
                                                <option value="Calidad Propuesta Tecnica y Costo">Calidad Propuesta Tecnica y Costo</option>
                                                <option value="Calidad">Calidad</option>
                                                <option value="Precio Evaluado mas Bajo">Precio Evaluado mas Bajo</option>
                                        </c:if>
                                    </select>
                                <input type="text" id="dias1" name="dias1" placeholder="PLAZO DE ENTERGA" value="${dias1}">
                                <%--input type="text" id="dias2" name="dias2" placeholder="DIAS V. PROPUESTA" value="${dias2}"></td--%>
                                <td><input type="text" id="nom" name="nom" placeholder="Nombre" value="${nom}">
                                    <input type="text" id="cargo" name="cargo" placeholder="Cargo" value="${cargo}">
                                    <input type="text" id="correo" name="correo" placeholder="Correo" value="${correo}">
                                </td>
                                <c:if test="${cuantia=='ANPE > 200000'}">
                                <td>
                                    <input type="checkbox" id="id1" name="id1" ${uno}> Inspecciones previas<BR>
                                    <input type="checkbox" id="id2" name="id2" ${dos}> Consultas escritas<BR>
                                    <input type="checkbox" id="id3" name="id3" ${tres}> Reunion de aclaracion<BR>
                                </td>
                                </c:if>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </c:if>
            <%--div class="box">
                
                <h2>
                    <a href="#" id="toggle-forms">Form. de Articulos</a>
                </h2>   
                
                <form id="formu_prob" name='forma' action='<c:url value="/TransaccionDetalleItems.umsa"/>' method='post' enctype="">
                    <input name="usuario_sol" type="hidden" value="<c:out value="${usuario_sol}"/>"/>
                    <input type="hidden" name="detalle" value="${detalle}" />
                    <input name="ue_solicitante" type="hidden" value="<c:out value="${ue_solicitante}"/>" />
                    <input name="ue_destino" type="hidden" value="<c:out value="${ue_destino}"/>" />
                    <input type=hidden name="cod_transaccion" value="<c:out value="${cod_transaccion}"/>" >
                    <input type=hidden name=tipo_tramite value="<c:out value="${tipo_tramite}"/>" >
                    <input type=hidden name=cod_tramite value="<c:out value="${cod_tramite}"/>" >
                    <input type=hidden name=cod_w value="<c:out value="${cod_w}"/>" >
                    <input type=hidden name=nro_gestion value="<c:out value="${nro_gestion}"/>" >
                    <input type=hidden name=cuantia value="<c:out value="${cuantia}"/>" >
                    
                     Buscar Item : <input type="text" name="search" value="${search}" id="search">
                     <input type="submit" name="opcion" value="Buscar" id="Buscar" onclick="return setBuscar()" >
                </form>
                
                <button id="busqueda_avanzada_items">Busqueda Avanzada</button>
                <table cellpadding="0" cellspacing="0" border="0" style="width:100%">
                    <tr>
                        <td class="gridContent">
                            <div id="scrolltable" style=" overflow:auto; padding-top:5px; padding-bottom: 15px; height:100px; top:20px" >
                                <table id="tabla">
                                    <thead>
                                        <tr>
                                            <td class="gridHeaders">PARTIDA</td>
                                            <td class="gridHeaders">TIPO ITEM</td>
                                            <td class="gridHeaders">ITEM</td>
                                            <td class="gridHeaders">UNID.MEDIDA</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:if test="${empty items}">
                                            <tr>
                                                <td colspan="4">No se han encontrado elementos.</td>
                                            </tr>
                                        </c:if>                                        
                                        <c:forEach var="item" items="${items}">                                       
                                            <tr onclick="javascript:return CantidadItem('${item.cod_item}','${item.unidad_medida}')" style="cursor:pointer"  >
                                                <td class="gridData">${item.partida}</td>
                                                <td class="gridData">${item.tipo_item}</td>
                                                <td class="gridData">${item.articulo}</td>
                                                <td class="gridData">${item.unidad_medida}</td>
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                </table>                                                          
                            </div>
                            
                            <c:if test="${cod_tramite==1}">                            
                                <!--input type="button" value="Terminar" onclick="history.back()" style="font-family: Verdana; font-size: 10 pt"-->
                                <a href="/capri-web/TransaccionMateriales.umsa?cod_tramite=1&cod_w=${cod_w}"><button>Terminar</button></a>
                            </c:if>
                            <c:if test="${cod_tramite==4}">                            
                                <!--input type="button" value="Terminar" onclick="history.back()" style="font-family: Verdana; font-size: 10 pt"-->
                                <a href="/capri-web/TransaccionMateriales.umsa?cod_tramite=1&cod_w=${cod_w}"><button>Terminar</button></a>
                            </c:if>
                        </td>
                    </tr>
                </table>            
            </div--%>
                            <c:if test="${cod_tramite==1}">                            
                                <!--input type="button" value="Terminar" onclick="history.back()" style="font-family: Verdana; font-size: 10 pt"-->
                                <a href="javascript:datosAnpe('${cuantia}');"><button>Terminar</button></a>
                            </c:if>
                            <c:if test="${cod_tramite==4}">                            
                                <!--input type="button" value="Terminar" onclick="history.back()" style="font-family: Verdana; font-size: 10 pt"-->
                                <a href="/capri-web/TransaccionPedidos.umsa?cod_tramite=4&cod_w=${cod_w}"><button>Terminar</button></a>
                            </c:if>
            <div id="dialog_poa" title="PRESUPUESTO">
                <table>
                    <thead>
                        <th>PARTIDA</th>
                        <th>DESCRIPCCION</th>
                        <th>FTE</th>
                        <th>ORG</th>
                        <th>PRESUPUESTO</th>
                        <th>EJECUTADO</th>
                        <th>SALDO</th>
                    </thead>
                    <tbody>
                    <tr>
                        <td>1</td>
                        <td>2</td>
                        <td>3</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="sample_popup"     id="popup" style="display: none;">
                <div class="menu_form_body">
                    <form action='<c:url value="/TransaccionArticuloGuarda.umsa"/>' method='post' name='forma1' onsubmit="return setRegistrar(this)">
                        <table>
                            <tr>
                                <td>
                                    Cantidad222 :
                                </td>
                                <td>
                                    <input class="field" type="text" onfocus="select();" name="cantidad"/>
                                    <input type=hidden id="cod_item" name="cod_item">
                                    <input type=hidden id="unidad_medida" name="unidad_medida">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" >
                                    <center>                                                                
                                        <input type=hidden name=tipo_tramite value="<c:out value="${tipo_tramite}"/>" >
                                        <input type=hidden name=cod_tramite value="<c:out value="${cod_tramite}"/>" >
                                        <input type=hidden name=usuario_sol  value="<c:out value="${usuario_sol}"/>"/>
                                        <input type=hidden name=detalle  value="<c:out value="${detalle}"/>"/>
                                        <input type=hidden name=ue_solicitante value="<c:out value="${ue_solicitante}"/>" />
                                        <input type=hidden name=ue_destino value="<c:out value="${ue_destino}"/>" />
                                        <input type=hidden name=nro_gestion value="<c:out value="${nro_gestion}"/>" />
                                        <input type=hidden name=cod_transaccion value="<c:out value="${cod_transaccion}"/>" >
                                        <input type=hidden name=cod_w value="<c:out value="${cod_w}"/>" >
                                    <input type=submit value="Aceptar" >
                                    </center>                             
                                </td>
                            </tr>
                        </table>
                    </form>
                </div>
            </div>
            <div id="buscador_itemsAvanzado" class="ui-dialog-content ui-widget-content" title="Busqueda Avanzada de Articulos">
                <div id="header_buscadorAvanzado">
                    <div id="busca_partida">
                        <select name="thelist" id="combo_partidas">
                            <option value="0">Escoja una Partida</option>
                            <c:forEach var="lista" items="${lista_partidas.pageList}" varStatus="contador">
                                 <option value = <c:out value="${lista.partida}"/>><c:out value="${lista.detalle_partida}"/></option>
                             </c:forEach>
                        </select>
                        
                    </div>
                    <div id="descripcion_partida" >
                        <h3>Descripción de la Partida</h3>
                        <p>
                            Aqui va la descripción de la partida
                            
                        </p>
                    </div>
                </div>
		
                <div id="cuerpo_buscadorAvanzado">
                    <table>
                        <thead>
                            <tr>
                                <td>ITEM</td>
                                <td>DETALLE</td>
                                <td>UNIDAD DE MEDIDA</td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            
            <div id="dialog_carga" title="Espere Por Favor"><img src="/capri-web/imagenes/cargando.gif" alt="Cargando....." height="150" width="auto"></div>
            <div id="dialog_no-item" title="¿Qué hago si no encuentro el item?"><img src="/capri-web/imagenes/no-item.jpg" height="370" width="auto"/></div>
            <div id="dialog_detalle_item" title="Detalle adicional">
                 <!--textarea rows="4" cols="50">At w3schools.com you will learn how to make a website.</textarea-->
                 <table>
                     <thead>
                         <tr>
                             <th>Complemento</th>
                             <th>Opciones</th>
                         </tr>
                     </thead>
                     <tbody>
                         
                     </tbody>
                 </table>
                 <!--a id="addDetalle" href="#" ><img class="img_peque" src="imagenes/plus.png" alt="addDetalle"/></a-->
            </div>
            <div id="dialog_unidad_medida" title="Unidad de Medida">
                <input id="val_unidad_medida" data-cod_unidad_medida="" type="text" size="40" maxlength="40" placeholder="Ingrese descripción unidad de medida"/>
            </div>
            <script src="js/tablaDinamica.js"></script>
          
    </body>
</html>
